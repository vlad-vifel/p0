from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=120, blank=False)
    describtion = models.CharField(max_length=620, blank=False)
    price = models.IntegerField(default=0)

    def __str__(self):
        return self.title